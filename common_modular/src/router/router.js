const _import = require('@/router/_import')

// 作为Main组件的子页面展示并且在左侧菜单显示的路由写在appRouter里
export const appRouter = [
    {
        path: '/',
        redirect: '/home'
    },
    {
        path: '/login',
        name : 'login',
        component: _import('login'),
    },
    {
        path: '/home',
        name : '首页',
        component: _import('home/index'),
    },
    {
        path: '/type',
        name : '分类',
        component: _import('type/index'),
    },
    {
        path: '/shopCart',
        name : '购物车',
        component: _import('shopCart/index'),
    },
    {
        path: '/person',
        name : '个人中心',
        component: _import('person/index'),
    },
    {
        path: '/register',
        name : '注册',
        component: _import('register/register'),
    },

];

// 所有上面定义的路由都要写在下面的routers里
export const routers = [
    // loginRouter,
    // ...otherRouter,
    ...appRouter
];

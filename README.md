# learningMaterials

#### 介绍
项目资料

#### 软件架构
软件架构说明
1.common_modular文件夹为vue项目源码  基于Vant框架

#### 安装教程

1. 初次在common_modular根目录 终端输出 cnpm installan安装依赖
2. npm run dev运行项目
3. npm run build打包项目

#### 使用说明

1. UI蓝湖地址
	https://lanhuapp.com/web/#/item/project/board?pid=28a82285-bd11-4ed3-9b5d-088ad22022a1
	账号 978493475@qq.com	
	密码 guan2016
2. 实现页面 （登录、首页、分类、购物车、个人中心）
3. 学习Vant组件 官方地址https://youzan.github.io/vant/1.x/#/zh-CN/intro

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)